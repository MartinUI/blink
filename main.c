/* 
 * File:   main.c
 * Author: marcin
 * 
 * Created on marca 22, 2018, 08:00 PM
 */

#ifndef F_CPU
#define F_CPU 1000000
#endif


#include <avr/io.h>
#include <util/delay.h>




int main(void) {
    
    DDRB = 0x03;
//    PORTB = 0x01;
    
    for(;;){
    PORTB = 0x01;
    _delay_ms(250);
    PORTB = 0;
    _delay_ms(250);
        
    }
    return 0;
    
}
